import React from 'react'
import './ButtonLogin.css'

class LoginButton extends React.Component {
    render() {
        return <button className="button-login">{this.props.buttonMessage}</button>
    }
}

export default LoginButton