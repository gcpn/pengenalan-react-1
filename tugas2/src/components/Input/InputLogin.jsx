import React from 'react'
import './InputLogin.css'

class InputLogin extends React.Component {
    render() {
        return (
            <div className="form-login">
                <input type="text" placeholder={this.props.userPlaceholder}/><br/>
                <input type="password" placeholder={this.props.passPlaceholder}/>
            </div>
        )
    }
}

export default InputLogin