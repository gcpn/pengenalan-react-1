import React from 'react';
import logo from './logo.svg';
import './App.css';
import ButtonLogin from './components/Button/ButtonLogin'
import InputLogin from './components/Input/InputLogin'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>Selamat datang di Aplikasi React</h2>
        <InputLogin userPlaceholder="Email Anda" passPlaceholder="Password Anda"/>
        <ButtonLogin buttonMessage="Login"/>
      </header>
    </div>
  );
}

export default App;