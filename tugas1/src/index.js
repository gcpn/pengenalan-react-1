import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';

// pembuatan elemen h1 dengan createElement()
// dengan 2 atribut
const MainHeader = React.createElement(
  'h1',
  {
    id: 'main-header',
    title: 'ini adalah title untuk main header'
  },
  'Header utama'
);

const imgSrc = 'https://www.w3schools.com/html/pic_trulli.jpg'
const imgWidth = '100px'
const imgHeight = '100px'

// contoh elemen dengan child: img di dalam p
// contoh ini juga menggunakan ekspresi
const MainImg = (
  <p>
    <img src={imgSrc} width={imgWidth} height={imgHeight}/>
  </p>
)

// render elemen yang sudah dibuat pada div dengan id = root
ReactDOM.render(
  MainHeader,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
